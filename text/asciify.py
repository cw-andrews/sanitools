import string


def asciify(line: str) -> str:
    """
    Return printable ascii characters + TAB ignoring all others.
    """

    if isinstance(line, str):

        good_characters = set(string.digits +
                              string.ascii_letters +
                              string.punctuation +
                              '\t' +
                              ' ')

        return ''.join(char for char in line if char in good_characters)
    else:
        raise TypeError("line variable MUST be str.")
