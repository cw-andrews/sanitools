import pytest

from text.asciify import asciify


class TestAsciify(object):
    def test_required_args(self):
        with pytest.raises(TypeError):
            asciify()
        with pytest.raises(TypeError):
            asciify(1)

    def test_return_values(self):
        assert isinstance(asciify("Test Text"), str)
        # assert TEST_URL in combinator(TEST_URL, TEST_UTM_CODES)
        # assert TEST_UTM_CODES in combinator(TEST_URL, TEST_UTM_CODES)


if __name__ == '__main__':
    pytest.main()
