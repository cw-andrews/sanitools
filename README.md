Make it Yours
=============
```
git clone https://gitlab.com/cw-andrews/project_template.git $YOUR_PROJECT_NAME
cd $YOUR_PROJECT_NAME
rm -rf .git/
```
Install Requirements via pip
============================
```
pip install -r requirements.txt
```

Run Tests via pytest
====================
```
pytest -v .
```

Use
====================
```
Put some code here.
```
